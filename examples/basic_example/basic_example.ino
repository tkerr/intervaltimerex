/******************************************************************************
 * basic_example.ino
 * Copyright (c) 2021 Thomas Kerr AB3GY
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief Example sketch demonstrating basic use of the IntervalTimerEx library.
 */
 
/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <Arduino.h>


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "IntervalTimerEx.h"


/******************************************************************************
 * Forward references.
 ******************************************************************************/
void ledHandler();    //!< Event handler to turn LED on and off
void loopHandler();   //!< Event handler to print timestamps in main loop


/******************************************************************************
 * Local definitions.
 ******************************************************************************/
#define SERIAL_BAUD  (115200)


/******************************************************************************
 * Global objects and data.
 ******************************************************************************/
IntervalTimerEx myLedTimer;   //!< The LED demonstration timer
IntervalTimerEx myLoopTimer;  //!< The main loop demonstration timer

const uint32_t LED_PERIOD_US  = 2000000;  //!< The LED service interval in microseconds
const uint32_t LOOP_PERIOD_US = 5000000;  //!< The print service interval in microseconds

int      led_state = 0;             //!< LED on/off state
bool     led_stopped = false;       //!< Used to demonstrate timer stop/restart
bool     led_restarted = false;     //!< Used to demonstrate timer stop/restart
bool     loop_timer_ready = false;  //!< Loop timer interrupt flag
uint32_t loop_timer_count = 0;      //!< Loop timer interrupt count




/******************************************************************************
 * Local data.
 ******************************************************************************/


/******************************************************************************
 * Public functions.
 ******************************************************************************/

/**************************************
 * setup()
 **************************************/ 
void setup()
{
    // Hardware initialization.
    led_state = 0;
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, led_state);
    Serial.begin(SERIAL_BAUD);
    delay(100);
    Serial.println("IntervalTimerEx basic example sketch");

    // Initialize the timers.
    if (!myLedTimer.begin(ledHandler, LED_PERIOD_US))
    {
        Serial.println("myLedTimer initialization failed.");
        while (true) {}
    }
    myLedTimer.start();
    
    if (!myLoopTimer.begin(loopHandler, LOOP_PERIOD_US))
    {
        Serial.println("myLoopTimer initialization failed.");
        while (true) {}
    }
    loop_timer_ready = false;
    myLoopTimer.start();
}


/**************************************
 * loop()
 **************************************/ 
void loop()
{
    uint32_t now = millis();
    
    // Check if flag was set by interval timer interrupt.
    if (loop_timer_ready)
    {
        loop_timer_ready = false;
        Serial.print("Loop timer ");
        Serial.print(loop_timer_count);
        Serial.print(" expired at ");
        Serial.print(now);
        Serial.println(" ms.");
    }
    
    // Demonstrate the ability to stop and restart the timer.
    // Stop it after 20 seconds and restart it with a new
    // interval after 30 seconds.
    if ((now >= 20000) && !led_stopped)
    {
        Serial.println("Stopping the LED timer for 10 seconds.");
        myLedTimer.stop();
        led_state = 0;
        digitalWrite(LED_BUILTIN, led_state);
        led_stopped = true;
    }
    if ((now >= 30000) && !led_restarted)
    {
        Serial.println("Restarting the LED timer with a faster interval.");
        myLedTimer.setInterval(LED_PERIOD_US >> 2);
        myLedTimer.start();
        led_restarted = true;
    }
}


/**************************************
 * ledHandler()
 **************************************/ 
void ledHandler()
{
    // Since this is run from an interrupt, it should be kept simple and quick.
    led_state = 1 - led_state;
    digitalWrite(LED_BUILTIN, led_state);
}


/**************************************
 * loopHandler()
 **************************************/ 
void loopHandler()
{
    // Set a flag and to handle timer expiration in the main loop.
    // Serial operations should not occur within interrupts.
    // Since this is run from an interrupt, it should be kept simple and quick.
    loop_timer_ready = true;
    loop_timer_count++;
}


/******************************************************************************
 * Private functions.
 ******************************************************************************/

// End of file.