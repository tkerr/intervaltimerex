# IntervalTimerEx #
Interval Timer with Extensions.

Based on IntervalTimer from Teensyduino Core Library.  
See https://www.pjrc.com/teensy/td_timing_IntervalTimer.html for details.

Note that **advanced programming is required** to properly use IntervalTimerEx, because your function runs as an interrupt. 
See the IntervalTimer documentation for details. 

Summary of modifications:
  1. After calling begin(), timer is in a stopped state.  
  2. Added start() method to start timer.  
  3. Added stop() method to stop timer.  
  4. Added setInterval() method to stop timer and change the interval.  
     Must start() timer after calling this method.  
     Note: use existing update() method to change the interval upon expiration of the current interval and keep going.
  5. Added count() method to return the current timer count.  

### Architectures ###
Currently only supported on teensy platforms from PJRC. See URL below. 

### Author ###
Based on IntervalTimer from Teensyduino Core Library  
http://www.pjrc.com/teensy/  
Copyright (c) 2017 PJRC.COM, LLC.  

Modifications by Tom Kerr AB3GY (ab3gy@arrl.net).  

### License ###
See license.txt for details.
